package com.paris.bibliotheque.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.paris.bibliotheque.model.Livre;
import com.paris.bibliotheque.service.LivreService;

@RestController
@RequestMapping("/livres")
public class LivreController {

	@Autowired
	private LivreService livreService;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/identifiant")
	public Livre getLivre(@RequestParam(value = "isbn", defaultValue = "1") Long isbn) {
		return livreService.getLivreById(isbn);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("")
	public List<Livre> getLivres(){
		return livreService.getAllLivres();
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "", consumes = "application/json")
	public Livre nouveauLivre(@RequestBody Livre livre) {
		return livreService.createLivre(livre);
	}
		
}
