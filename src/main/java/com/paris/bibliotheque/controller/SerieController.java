package com.paris.bibliotheque.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.paris.bibliotheque.model.Serie;
import com.paris.bibliotheque.service.SerieService;

@RestController
@RequestMapping("/series")
public class SerieController {

	@Autowired
	private SerieService serieService;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/identifiant")
	public Optional<Serie> getSerieByIdentifiant(@RequestParam(value = "id", defaultValue = "1") Long id){
		return serieService.getSerieByIdentifiant(id);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("")
	public List<Serie> getSeries(){
		return serieService.getSeries();
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/detail")
	public List<Serie> getSeriesDetaillee(){
		return serieService.getSeriesDetaillee();
	}

	@GetMapping("/detail/identifiant")
	public Serie getSerieDetaillee(@RequestParam(value = "id", defaultValue = "1") Long id) {
		return serieService.getSerieDetaille(id);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "", consumes = "application/json")
	public Serie nouvelleSerie(@RequestBody Serie serie) {
		return serieService.createSerie(serie);
	}
	
}
