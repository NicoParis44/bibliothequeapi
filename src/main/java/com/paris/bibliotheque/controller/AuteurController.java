package com.paris.bibliotheque.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.paris.bibliotheque.model.Auteur;
import com.paris.bibliotheque.service.AuteurService;

@RestController
@RequestMapping("/auteurs")
public class AuteurController {

	@Autowired
	private AuteurService auteurService;
	
	@CrossOrigin(origins = "*")
	@GetMapping("/identifiant")
	public Optional<Auteur> getAuteurByIdentifiant(@RequestParam(value = "id", defaultValue = "1") Long identifiant){
		return auteurService.getAuteurByIdentifiant(identifiant);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/detail/identifiant")
	public Auteur getAuteurDetailleByIdentifiant(@RequestParam(value = "id", defaultValue = "1") Long identifiant){
		return auteurService.getAuteurWithRichData(identifiant);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("")
	public Iterable<Auteur> getAuteurs(){
		return auteurService.getAuteurs();
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/detail")
	public List<Auteur> getAuteursDetaille(){
		return auteurService.getAuteursWithRichData();
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping(value = "", consumes = "application/json")
	public Auteur nouvelAuteur(@RequestBody Auteur auteur) {
		System.out.println(auteur.getNom());
		return auteurService.createAuteur(auteur);
	}
}
