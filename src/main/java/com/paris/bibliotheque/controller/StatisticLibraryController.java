package com.paris.bibliotheque.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.paris.bibliotheque.service.StatisticLibraryService;

@RestController
@RequestMapping("/statisticsLibrary")
public class StatisticLibraryController {
	
	@Autowired
	private StatisticLibraryService statisticService;
	
		
	@CrossOrigin(origins = "*")
	@GetMapping("nblivreByGenre")
	public List<Object> getNbLivresByGenre(){
		return statisticService.getNbLivresByGenre();
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("avancementLecture")
	public List<Object> getProgressReading(){
		return statisticService.getProgressReading();
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("statAuteur")
	public List<Object> getStatAuteurReading(){
		return statisticService.getStatAuteurReading();
	}

}
