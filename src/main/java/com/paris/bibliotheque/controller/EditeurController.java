package com.paris.bibliotheque.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.paris.bibliotheque.model.Editeur;
import com.paris.bibliotheque.service.EditeurService;

@RestController
@RequestMapping("editeurs")
public class EditeurController {
	
	@Autowired
	private EditeurService editeurService;
	
	@GetMapping("")
	public Iterable<Editeur> getEditeurs(){
		return editeurService.getEditeurs();
	}
	
	@GetMapping("/identifiant")
	public Optional<Editeur> getEditeur(@RequestParam(value = "id", defaultValue = "1") Long id){
		return editeurService.getEditeurByIdentifiant(id);
	}

}
