package com.paris.bibliotheque.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.paris.bibliotheque.model.Editeur;

@Repository
public interface EditeurRepository extends CrudRepository<Editeur, Long> {

}
