package com.paris.bibliotheque.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.paris.bibliotheque.model.Livre;

@Repository
public interface LivreRepository extends CrudRepository<Livre, Long> {

	@Query("SELECT l FROM Livre l WHERE l.identifiantAuteur = ?1")
	public List<Livre> getLivresForAuteur( Long idAuteur);
	
	@Query("SELECT l FROM Livre l WHERE l.identifiantSerie = ?1 ORDER BY l.numeroTome ASC")
	public List<Livre> getLivresForSerie( Long idSerie);
}
