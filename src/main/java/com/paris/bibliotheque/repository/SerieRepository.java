package com.paris.bibliotheque.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.paris.bibliotheque.model.Serie;

@Repository
public interface SerieRepository extends CrudRepository<Serie, Long> {

	@Query("SELECT s FROM Serie s WHERE s.identifiantAuteur = ?1")
	public List<Serie> getSeriesByAuteur(Long idAuteur);
}
