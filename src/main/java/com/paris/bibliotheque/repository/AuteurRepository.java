package com.paris.bibliotheque.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.paris.bibliotheque.model.Auteur;

@Repository
public interface AuteurRepository extends CrudRepository<Auteur, Long> {

}
