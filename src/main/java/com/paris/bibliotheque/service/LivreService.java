package com.paris.bibliotheque.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paris.bibliotheque.model.Livre;
import com.paris.bibliotheque.repository.LivreRepository;

@Service
public class LivreService {

	@Autowired
	private LivreRepository livreRepository;
	@Autowired
	private AuteurService auteurService;
	@Autowired
	private SerieService serieService;
	
	public Livre getLivreById(Long id){
		Livre livre = livreRepository.findById(id).get();
		livre.setAuteur(auteurService.getAuteurByIdentifiant(livre.getIdentifiantAuteur()).get());
		livre.setSerie(serieService.getSerieByIdentifiant(livre.getIdentifiantSerie()).get());
		return livre;
	}
	
	public List<Livre> getAllLivres(){
		List<Livre> livres = new ArrayList<Livre>();
		Iterable<Livre> resultQuery = livreRepository.findAll();
		for(Livre livre : resultQuery) {
			livres.add(livre);
		}
		
		return livres;
	}
	
	public Livre createLivre(Livre livre) {
		Livre newLivre = livreRepository.save(livre);
		return newLivre;
	}
	
}
