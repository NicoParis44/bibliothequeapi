package com.paris.bibliotheque.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paris.bibliotheque.model.Editeur;
import com.paris.bibliotheque.repository.EditeurRepository;

@Service
public class EditeurService {
	
	@Autowired
	private EditeurRepository editeurRepository;
	
	public Iterable<Editeur> getEditeurs(){
		return editeurRepository.findAll();
	}
	
	public Optional<Editeur> getEditeurByIdentifiant(Long id){
		return editeurRepository.findById(id);
	}

}
