package com.paris.bibliotheque.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.paris.bibliotheque.model.Auteur;
import com.paris.bibliotheque.repository.AuteurRepository;
import com.paris.bibliotheque.repository.LivreRepository;

import lombok.Data;

@Data
@Service
public class AuteurService {

	@Autowired
	private AuteurRepository auteurRepository;
	@Autowired
	private LivreRepository livreRepository;
	@Autowired
	private SerieService serieService;
	
	
	/**
	 * View detail of the specific author
	 * Not detail descriptor
	 * @param identifiant identifiant
	 * @return
	 */
	public Optional<Auteur> getAuteurByIdentifiant(Long identifiant) {
		return auteurRepository.findById(identifiant);
	}
	
	public List<Auteur> getAuteurs(){
		List<Auteur> auteurs = new ArrayList<Auteur>();
		auteurRepository.findAll().forEach(auteurs::add);
		return auteurs;
	}
	
	public Auteur getAuteurWithRichData(Long idAuteur) {
		Auteur auteur = auteurRepository.findById(idAuteur).get();
		if(auteur != null)
			auteur.setSeries(serieService.getSeriesWithLivres(idAuteur));
			
		return auteur;
	}
	
	public List<Auteur> getAuteursWithRichData(){
		List<Auteur> auteurs = new ArrayList<Auteur>();
		Iterable<Auteur> result = auteurRepository.findAll();
		for(Auteur auteur : result) {
			auteur.setSeries(serieService.getSeriesWithLivres(auteur.getId()));
			auteurs.add(auteur);
		}
		return auteurs;
	}
	
	public Auteur createAuteur(Auteur auteur) {
		auteur.setPrenom(StringUtils.capitalize(auteur.getPrenom().toLowerCase()));
		auteur.setNom(auteur.getNom().toUpperCase());
		auteur.setNationalite(StringUtils.capitalize(auteur.getNationalite().toLowerCase()));
		Auteur newAuteur = auteurRepository.save(auteur);
		return newAuteur;
	}
	
}
