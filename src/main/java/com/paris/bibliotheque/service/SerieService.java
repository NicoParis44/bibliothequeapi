package com.paris.bibliotheque.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paris.bibliotheque.model.Serie;
import com.paris.bibliotheque.repository.AuteurRepository;
import com.paris.bibliotheque.repository.LivreRepository;
import com.paris.bibliotheque.repository.SerieRepository;

@Service
public class SerieService {

	@Autowired
	private SerieRepository serieRepository;
	
	@Autowired
	private AuteurRepository auteurRepository;
	
	@Autowired
	private LivreRepository livreRepository;
	
	public Optional<Serie> getSerieByIdentifiant(Long id){
		Optional<Serie> serie = serieRepository.findById(id);
		return serie;
	}
	
	public List<Serie> getSeries(){
		List<Serie> series = new ArrayList<Serie>();
		Iterable<Serie> resultQuery = serieRepository.findAll();
		for(Serie serie : resultQuery) {
			series.add(serie);
		}
		return series;
	}
	
	public List<Serie> getSeriesWithLivres(Long idAuteur){
		List<Serie> series = serieRepository.getSeriesByAuteur(idAuteur);
		for(Serie  s: series) {
			s.setLivres(livreRepository.getLivresForSerie(s.getId()));
		}
		return series;
	}
	
	public List<Serie> getSeriesDetaillee(){
		Iterable<Serie> resultQuery = serieRepository.findAll();
		List<Serie> series = new ArrayList<Serie>();
		for(Serie serie : resultQuery) {
			serie.setAuteur(
					auteurRepository.findById(serie.getIdentifiantAuteur()).get());
			serie.setLivres(
					livreRepository.getLivresForSerie(serie.getId()));
			series.add(serie);
		}
		return series;
	}
	
	public Serie getSerieDetaille(Long id) {
		Serie serie = getSerieByIdentifiant(id).get();
		serie.setLivres(livreRepository.getLivresForSerie(id));
		return serie;
	}
	
	public Serie createSerie(Serie serie) {
		Serie newSerie = serieRepository.save(serie);
		return newSerie;
	}
}
