package com.paris.bibliotheque.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
	
import org.springframework.stereotype.Service;

@Service
public class StatisticLibraryService {
	
//	@Autowired
//	private StatisticLibraryRepository statisticRepository;
	
	@PersistenceContext
	EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public List<Object> getNbLivresByGenre() {
		List<Object> objects = new ArrayList<Object>();
		objects.add(Arrays.asList("Genre", "Nombre de Livres"));
		objects.addAll(entityManager.createNativeQuery("SELECT Genre, COUNT(titre) AS NbLivres FROM Livre GROUP BY Genre;").getResultList());
		return objects;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getProgressReading() {
		List<Object> objects = new ArrayList<Object>();
		objects.add(Arrays.asList("Genre", "Nombre de Livres Théorique", "Nombre de Livres Réel", "Possède", "Lu"));
		objects.addAll(entityManager.createNativeQuery("SELECT Genre, NbLivreTheorique, NbLivresReel, possede, lu FROM STAT_POSSESSION;").getResultList());
		return objects;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getStatAuteurReading() {
		List<Object> objects = new ArrayList<Object>();
		objects.add(Arrays.asList("auteur", "Nombre de Séries", "Nombre de Livres", "Nombre de Livres Lus"));
		objects.addAll(entityManager.createNativeQuery("SELECT auteur, NbSerie, NbLivres, NbLivresLu FROM STAT_AUTEURS;").getResultList());
		return objects;
	}
	
}
