package com.paris.bibliotheque.model;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@Entity
@Table(name="serie")
public class Serie {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="idauteur")
	private Long identifiantAuteur;
	
	@Transient
	@JsonInclude(Include.NON_NULL)
	private Auteur auteur;
	
	@Column(name="nombretome")
	private Long nombreTome;
	
	@Column(name="titre")
	private String titre;
	
	@Transient
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<Livre> livres;
	
	@Column(name="genre")
	private String genre;
}
