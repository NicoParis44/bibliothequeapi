package com.paris.bibliotheque.model;

import jakarta.persistence.*;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.ModelAttribute;

@Data
@Getter
@Setter
@Entity
@Table(name="livre")
public class Livre {
	
	@Id
	@Column(name="isbn")
	@JsonProperty("isbn")
	private Long isbn;
	
	@Column(name="titre")
	@JsonProperty("titre")
	private String titre;
	
	@Column(name="genre")
	@JsonProperty("genre")
	private String genre;
	
	@Column(name="numerotome")
	@JsonProperty("numeroTome")
	private int numeroTome;
	
	@Column(name="possede")
	@JsonProperty("possede")
	private boolean possede;
	
	@Column(name="lu")
	@JsonProperty("lu")
	private boolean lu;
	
	@Column(name="format")
	@JsonProperty("format")
	private String format;
	
	@Column(name="idAuteur")
	@JsonProperty("identifiantAuteur")
	private Long identifiantAuteur;

	@Transient
	@JsonInclude(Include.NON_NULL)
	private Auteur auteur;
	
	@Column(name="idSerie")
	@JsonProperty("identifiantSerie")
	private Long identifiantSerie;
	
	@Transient
	@JsonInclude(Include.NON_NULL)
	private Serie serie;
}
